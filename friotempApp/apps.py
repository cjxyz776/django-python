from django.apps import AppConfig


class FriotempappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'friotempApp'
