import pandas as pd

from sqlalchemy import create_engine

def get_data(table_name):
    db_connection_str = 'mysql+pymysql://dbuser:zkp3und9uec7CEG.jae@54.224.178.247/proyectospc4'
    db_connection = create_engine(db_connection_str)

    df = pd.read_sql('SELECT * FROM ' + table_name, con=db_connection)
    return df

def get_info(df_proyectos):
    #importar archivos excel
    df1 = get_data("consulta_amigable")
    columnas = ["CU", "Proyecto", "PIA", "PIM", "Devengado"]
    df_consulta = df1[columnas]
    #df_consulta = pd.read_excel(path, sheet_name="consulta_amigable", names=["CU","Proyecto","PIA","PIM","Devengado"])


    #df_inv = pd.read_excel("imversion.xlsx", sheet_name = "inversion", usecols = "B:L",
    #                       names=["UNIDAD","CU","COMPONENTES","COSTO_TOTAL","FUENTE",
    #                           "PROYECCION_GASTO_2022","2023","2024","2025","2026","2027"])
    columnas_entrada = ["UNIDAD_EJECUTORA", "CU", "COMPONENTES", "COSTO_TOTAL", "FUENTE", "PROYECCION_GASTO_2022", "a2023", "a2024", "a2025", "a2026", "a2027"]
    columnas_salida = ["UNIDAD","CU","COMPONENTES","COSTO_TOTAL","FUENTE", "PROYECCION_GASTO_2022","2023","2024","2025","2026","2027"]
    
    df1 = get_data("inversion")
    df2 = df1[columnas_entrada]
    df2.columns = columnas_salida
    df_inv = df2
    df_inv=df_inv.fillna(0)

    df_proy = df_proyectos

    #agregar columanas de costos x recursos
    df_recursos = pd.get_dummies(df_inv["FUENTE"])
    df_recursos = df_recursos[["Recursos Ordinarios","Recursos por Operaciones Oficiales de Crédito"]]

    df_inv_final = pd.concat([df_inv,df_recursos], axis = 1)
    df_inv_final["Recursos Ordinarios"] = df_inv_final["Recursos Ordinarios"]*df_inv_final["COSTO_TOTAL"]
    df_inv_final["Recursos por Operaciones Oficiales de Crédito"] = df_inv_final["Recursos por Operaciones Oficiales de Crédito"]*df_inv_final["COSTO_TOTAL"]

    #combinacion de dataframes
    df_proy_cons = pd.merge(
        df_proy,
        df_consulta,
        how="left",
    )

    df_inv_agreg = df_inv_final.groupby(by="CU").sum()
    df_inv_agreg = df_inv_agreg.reset_index()

    #creacion del df final
    df_final = pd.merge(
        df_proy_cons,
        df_inv_agreg,
        how="left",
    )
    ult_fila = df_final.shape[0]
    df_final = df_final.replace({"": 0})
    df_final = df_final.fillna(0)
    df_final["CU"] = [str(i) for i in df_final["CU"].to_list()]
    df_final["PIA"] = df_final["PIA"].astype("float64")
    df_final["PIM"] = df_final["PIM"].astype("float64")
    df_final["Devengado"] = df_final["Devengado"].astype("float64")
    df_final["Recursos Ordinarios"] = df_final["Recursos Ordinarios"].astype("float64")
    df_final["COSTO_TOTAL"] = df_final["COSTO_TOTAL"].astype("float64")
    df_final["PROYECCION_GASTO_2022"] = df_final["PROYECCION_GASTO_2022"].astype("float64")
    df_final["2023"] = df_final["2023"].astype("float64")
    df_final["2024"] = df_final["2024"].astype("float64")
    df_final["2025"] = df_final["2025"].astype("float64")
    df_final["2026"] = df_final["2026"].astype("float64")
    df_final["2027"] = df_final["2027"].astype("float64")
    df_final["Recursos por Operaciones Oficiales de Crédito"] = df_final["Recursos por Operaciones Oficiales de Crédito"].astype("int64")
    df_final = df_final.append(df_final.sum(numeric_only=True), ignore_index=True)
    df_final.at[ult_fila,"CU"] = "Total"
    df_final.at[ult_fila,"Proyecto"] = len(df_final)-1
    
    df_final = df_final.rename(columns={'COSTO_TOTAL':'Costo Total','PROYECCION_GASTO_2022':"Proy. Gasto 2022"})
    
    columnasy = ['CU', 'Proyecto', 'PIA', 'PIM', 'Devengado',
                'Costo Total','Recursos Ordinarios', 
                'Recursos por Operaciones Oficiales de Crédito', 'Proy. Gasto 2022', '2023',
                '2024', '2025', '2026', '2027'] 
    df_final2 = df_final[columnasy]
     
    
    return df_final2
