from django.urls import path
from .import views

from django.conf import settings
from django.contrib.staticfiles.urls import static
from django.conf.urls.static import static

urlpatterns = [
    path('', views.subir_excel, name="solicitudes"),
    path('subir', views.subir_excel, name="subir_excel")

    
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
