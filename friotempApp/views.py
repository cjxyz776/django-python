from django.shortcuts import render
# Create your views here.

### RECIBIR ARCHIVO EXCEL
from django.shortcuts import render
from django.http import HttpResponse
import pandas
from . import consultas

def home(request):
    return render(request, 'crear-solicitud.html')

def subir_excel(request):
    if request.method == 'POST':
        
        archivo_excel = request.FILES["archivo_excel"]
        
        df = pandas.read_excel(archivo_excel, sheet_name="proyectos")
        
        reporte = consultas.get_info(df)
        
        html = reporte.to_html()
        
        context = { "data": html}
        context = { "data": reporte}
        return render(request, 'solicitudes-cliente.html', context)
    else:
        return render(request, 'crear-solicitud.html')