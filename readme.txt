Proyecto
Consulta de proyectos

Prerequisitos

1. Instalar Docker version 20.10.17
2. Instalar docker-compose version 1.29.2
3. Instalar Python3
4. Instalar django >> pip3 install Django==4.0.6

# Librerías de Python3
5. environ >> pip3 install django-environ
6. >> pip3 install pymysql
7. >> pip3 install sqlparse
8. >> pip3 install openpyxl
9. >> pip3 install sqlalchemy

python3 manage.py migrate
python3 manage.py createsuperuser